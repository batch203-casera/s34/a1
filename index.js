//Initialize dependencies and libraries
require('dotenv').config();
const express = require('express');
const port = 3000;
const app = express();

//Middleware
app.use(express.json());

//Mock Database
const arrays = require("./mock-db");

//App Method
app.get("/", (req, res) => {
    //combines writeHead() and end()
    res.send("Hello from my first REST API")
});

//Mini Activity
app.get("/greetings", (req, res) => {
    //combines writeHead() and end()
    res.send("Hello There!");
});

//Display users
app.get("/users", (req, res) => {
    //combines writeHead() and end()
    res.send(arrays.users);
});

//Adding a new user
app.post('/users', (req, res) => {
    let newUser = {
        username: req.body.username,
        email: req.body.email,
        password: req.body.password
    }
    arrays.users.push(newUser);
    res.send(arrays.users);
});

//Delete a user
app.delete('/users', (req, res) => {
    let newUser = {
        username: req.body.username,
    }
    arrays.users.splice(arrays.users.indexOf(newUser.username), 1);
    res.send(arrays.users);
});

//Update a user
app.put('/users/:index', (req, res) => {
    let index = req.params.index;
    let updatedUserName = {
        password: req.body.password,
    }

    arrays.users[index].password = updatedUserName.password;

    res.send(arrays.users[index]);
})

// ACTIVITY -----------------------------------------------------
// 
//
//

// GET ITEMS
app.get("/item", (req, res) => {
    //combines writeHead() and end()
    res.send(arrays.items);
});

// POST ITEM
app.post('/item', (req, res) => {
    let newItem = {
        name: req.body.name,
        price: req.body.price,
        isActive: req.body.isActive
    }
    arrays.items.push(newItem);
    res.send(arrays.items);
});

// PUT ITEM
app.put('/item/:index', (req, res) => {
    let index = req.params.index;
    let updatedItem = {
        price: req.body.price,
    }
    arrays.items[index].price = updatedItem.price;
    res.send(arrays.items[index]);
})

//Start Server Listen
app.listen(port, () => console.log("Server has started @port" + port));